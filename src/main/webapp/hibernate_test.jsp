<%@page import="ua.com.goit.volonteurs.solidcommunity.dao.common.HibernateUtil"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="UTF-8"%>
<%@ page import='java.sql.*' %>
<%@ page import='javax.sql.*' %>
<%@ page import='javax.naming.*' %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>JBOSS Application Server 7 &amp; JBOSS EAP</title>
</head>
<body>
<%
SessionFactory hiberFactory = null;
try {
    hiberFactory = HibernateUtil.getSessionFactory();
} catch (Exception ex) {
    System.out.println("Exception: " + ex + ex.getMessage());
}
%>
<%=hiberFactory.getStatistics().getConnectCount()%>
</body>
</html>