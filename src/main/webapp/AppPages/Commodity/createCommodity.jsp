<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="design.css">
<title>Commodity Creating page</title>
</head>
<body>

	<s:actionerror cssClass="errorTable" />

	<p>Please fill the forms</p>
	<s:form name="CommodityCreater" action="CommodityCreater">
		<table border="0">
			<tr>
				<td><s:textfield name="name" label="name" />
			<tr>
				<td><s:textfield name="price" label="season price" />
			<tr>
				<td><s:textfield name="nonSeasonPrice" label="non season price" />
			<tr>
				<td><s:textfield name="measure" label="measure" />
			<tr>
				<td><s:textarea name="description" label="description" />
				<td><s:submit />
			<tr>
		</table>
	</s:form>
    <a href="index.jsp">вернуться на главное меню</a>
</body>
</html>
