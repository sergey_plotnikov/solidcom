<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Editing</title>
</head>
<body>
	
	<s:actionerror cssClass="errorTable" />
    
    <table border="1" style="width: 60%">
		<tr>
			<td>id
			<td>Name
			<td>Price
			<td>Non season price
			<td>measure
			<td>description
		<tr>
			<s:form name="CommodityRetriver" theme="simple"
				action="CommodityRetriver">
				<tr>
					<td><s:property value="%{commodity.id}" />
					<td><s:property value="%{commodity.name}" />
					<td><s:property value="%{commodity.price}" />
					<td><s:property value="%{commodity.nonSeasonPrice}" />
					<td><s:property value="%{commodity.measure}" />
					<td><s:property value="%{commodity.description}" />
				</tr>
			</s:form>
		</tr>
			<s:form name="CommodityUpdater" action="CommodityUpdater">
				<td><s:textfield name="id" label="id" />
				<td><s:textfield name="name" label="name" />
				<td><s:textfield name="price" label="price" />
				<td><s:textfield name="nonSeasonPrice" label="nonSeasonPrice" />
				<td><s:textfield name="measure" label="measure" />
				<td><s:textfield name="description" label="description" />
				<td><s:submit />
			</s:form>
	</table>
    <a href="index.jsp">вернуться на главное меню</a>
</body>
</html>