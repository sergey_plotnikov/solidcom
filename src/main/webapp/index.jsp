<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Solid.com System</title>
	<link type="text/css" rel="stylesheet" href="design.css">
		
	  <style>
  		 h4, h5 {
			   margin-left: 20px; 
			}
	  </style>
  
    
</head>
<body>
  	
  	<s:actionerror cssClass="errorTable" />
    
  	<h3>Товары:</h3>
  	<h4><a href="<s:url action='CommodityList'/>"> Список товаров </a></h4>
	<h4><a href="<s:url action='CommodityFormCreater'/>"> Создать новый товар</a></h4>
	<h3>Запросы помощи:</h3>
	<h4><a href="<s:url action='RequestList'/>"> Список запросов </a></h4>
	<h4><a href="<s:url action='GetCommodities'/>"> Создать запрос о помощи </a></h4>
	<h3>Предложения о помощи:</h3>
	<h4><a href="<s:url action='ResponseList'/>"> Список предложений </a></h4>
	<h3>--------------------------------</h3>
	<h3>Прочее:</h3>
	<h4><a href="hibernate_test.jsp">hibernate_test.jsp</a></h4>
	
	
</body>
</html>