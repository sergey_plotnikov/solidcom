package ua.com.goit.volonteurs.solidcommunity.web.actions.response;

import com.opensymphony.xwork2.ActionSupport;

public class ResponseAcceptor extends ActionSupport {
    
    private static final long serialVersionUID = 5381057855033329026L;
    
    private String action;
    private long id;


    /* (non-Javadoc)
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        if ("accept".equals(action)) {
            //accept here
        } else if ("reject".equals(action)) {
            //reject here
        }
        return SUCCESS;
    }


    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }


    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }


    /**
     * @return the id
     */
    public long getId() {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    
}
