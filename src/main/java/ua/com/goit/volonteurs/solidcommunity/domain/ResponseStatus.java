package ua.com.goit.volonteurs.solidcommunity.domain;

public enum ResponseStatus {
    PENDING, ACCEPTED, REJECTED
}
