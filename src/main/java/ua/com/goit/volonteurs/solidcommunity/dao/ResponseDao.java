package ua.com.goit.volonteurs.solidcommunity.dao;

import ua.com.goit.volonteurs.solidcommunity.dao.common.PSQLGenericDao;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;

public class ResponseDao extends PSQLGenericDao<Response> {

	public ResponseDao() {
		super(Response.class);
	}

}
