/**
 * 
 */
package ua.com.goit.volonteurs.solidcommunity.domain;

import java.io.Serializable;

/**
 * @author SASH
 *
 */
public class Response implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6432988200300012870L;

    private long id;
    private String name;
    private String description;
    private ResponseStatus status;
    private Request request;

    // add required fields here

    public Response() {
        // TODO Auto-generated constructor stub
    }

    public Response fromRequest(Request request) {
        this.name = "Response to " + request.getName();
        this.request = request;
        return this;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * @return the status
     */
    public ResponseStatus getStatus() {
        return status;
    }

    /**
     * @return the request
     */
    public Request getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(Request request) {
        this.request = request;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

}
