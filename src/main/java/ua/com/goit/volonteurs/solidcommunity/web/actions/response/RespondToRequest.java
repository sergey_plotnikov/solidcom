package ua.com.goit.volonteurs.solidcommunity.web.actions.response;

import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.ResponseDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;
import ua.com.goit.volonteurs.solidcommunity.services.ResponseService;

import com.opensymphony.xwork2.ActionSupport;

public class RespondToRequest extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 4126509397847837969L;

    private long id;
    private long response_id;

    /**
     * @return the response_id
     */
    public long getResponse_id() {
        return response_id;
    }

    /**
     * @param response_id the response_id to set
     */
    public void setResponse_id(long response_id) {
        this.response_id = response_id;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        try {
            ResponseDao responceDao = ApplicationContextProvider
                    .getApplicationContext().getBean(ResponseDao.class);
            RequestDao requestDao = ApplicationContextProvider
                    .getApplicationContext().getBean(RequestDao.class);
            ResponseService responseService = new ResponseService();
            Response response = responseService.respondToRequest(requestDao
                    .retrieve(id));
            responceDao.save(response);
            this.response_id = response.getId();
            return SUCCESS;
        } catch (Exception e) {
            throw new SolidComDaoException("Can't respond to request", e);
        } finally {

        }
    }
}
