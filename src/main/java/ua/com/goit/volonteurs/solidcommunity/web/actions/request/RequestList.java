package ua.com.goit.volonteurs.solidcommunity.web.actions.request;

import java.util.List;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;
import ua.com.goit.volonteurs.solidcommunity.services.Paginator;

import com.opensymphony.xwork2.ActionSupport;

public class RequestList extends ActionSupport {

	private static final long serialVersionUID = 6435139451875594247L;
	private static final Logger LOG=Logger.getLogger(RequestList.class);
	// TODO need to change DAO-class to Service-class
	private RequestDao requestDao; 
	private Paginator paginator = new Paginator();
	private List<Request> requestList;
	private boolean delete;
	
	public String execute() throws Exception {
		
		paginator.checkAction();
		
		if(delete == true){
			try {
				delete();
				retrieve(paginator);
			} catch (SolidComDaoException e) {
				LOG.error("Can not delete Request: " + e.getMessage(), e);
				addActionError("Can not delete Request!");
			}
		} else { 
			try {
				retrieve(paginator);
			} catch (SolidComDaoException e) {
				LOG.error("Can not retrieve Request List: " + e.getMessage(), e);
				addActionError("Can not retrieve Request List!");
			}
		}
			
		return ActionSupport.SUCCESS;
	 }

	private void retrieve(Paginator paginator) throws SolidComDaoException {
		
		requestDao = ApplicationContextProvider.getApplicationContext().getBean(RequestDao.class);
		requestList = requestDao.retrieveAll(paginator);
		
	}

	
	private void delete() throws SolidComDaoException {
		// TODO Auto-generated method stub
		
	}

	public RequestDao getRequestDao() {
		return requestDao;
	}
	public void setRequestDao(RequestDao requestDao) {
		this.requestDao = requestDao;
	}
	public List<Request> getRequestList() {
		return requestList;
	}
	public void setRequestList(List<Request> requestList) {
		this.requestList = requestList;
	}
	public boolean getDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public Paginator getPaginator() {
		return paginator;
	}
	public void setPaginator(Paginator paginator) {
		this.paginator = paginator;
	}
	
}
