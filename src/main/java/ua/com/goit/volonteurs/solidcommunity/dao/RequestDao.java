package ua.com.goit.volonteurs.solidcommunity.dao;

import ua.com.goit.volonteurs.solidcommunity.dao.common.PSQLGenericDao;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;

public class RequestDao extends PSQLGenericDao<Request> {

	public RequestDao() {
		super(Request.class);
	}

}
