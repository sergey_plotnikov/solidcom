package ua.com.goit.volonteurs.solidcommunity.web.actions.request;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;

import com.opensymphony.xwork2.ActionSupport;

public class RequestDeleter extends ActionSupport {
	
	private static final long serialVersionUID = 7815623597711097759L;
	private static final Logger LOG = Logger.getLogger(RequestDeleter.class);
	
	private long id;
	private Request request;
		
	public RequestDeleter() {
		
	}
	
	public String execute(){
		RequestDao requestDao = ApplicationContextProvider.getApplicationContext().getBean(RequestDao.class);
		try {
			request = requestDao.retrieve(id);
		} catch (SolidComDaoException e) {
			LOG.error("Can't find request:" + e.getMessage(), e);
			addActionError("Can't find request!");
		}
		try {
			requestDao.delete(request);
		} catch (SolidComDaoException e) {
			LOG.error("Can't delete request:" + e.getMessage(), e);
			addActionError("Can't delete request!");
		}
		
		return ActionSupport.SUCCESS;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static Logger getLog() {
		return LOG;
	}
	
	

}
