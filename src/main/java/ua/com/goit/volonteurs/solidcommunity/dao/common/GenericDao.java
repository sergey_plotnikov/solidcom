/**
 * 
 */
package ua.com.goit.volonteurs.solidcommunity.dao.common;

import java.util.List;

import ua.com.goit.volonteurs.solidcommunity.services.Paginator;

/**
 * @author SASH
 *
 */
public interface GenericDao<T> {
    
    //create record in DB 
    public void save(T object) throws SolidComDaoException;
    
    //get object from DB
    public T retrieve(long id) throws SolidComDaoException;
    
    //update object in DB
    public void update(T object) throws SolidComDaoException;
    
    //delete object from DB
    public void delete(T object) throws SolidComDaoException;
    
    //get objects list from DB
    public List<T> retrieveAll() throws SolidComDaoException;
    
    //get paginated objects list from DB
    public List<T> retrieveAll(Paginator paginator) throws SolidComDaoException;
    
}
