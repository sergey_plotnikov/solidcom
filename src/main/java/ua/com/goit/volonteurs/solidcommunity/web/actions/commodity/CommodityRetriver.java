package ua.com.goit.volonteurs.solidcommunity.web.actions.commodity;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.CommodityDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;

import com.opensymphony.xwork2.ActionSupport;

public class CommodityRetriver extends ActionSupport {

	private static final long serialVersionUID = -8806285455060309352L;
	private static final Logger LOG = Logger.getLogger(CommodityRetriver.class);
	
	private String id;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	private Commodity commodity = null;
	
	public Commodity getCommodity() {
		return commodity;
	}
	
	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}
	
	public String execute() throws Exception {
		long parsedId = Long.parseLong(id);
		CommodityDao dao = new CommodityDao();
		try {
			commodity = dao.retrieve(parsedId);
		} catch (SolidComDaoException e) {
			LOG.error("Can not retrieve Commodity: " + e.getMessage(), e);
			addActionError("Can not retrive Commodity!");
			return ActionSupport.ERROR;
		}
		return ActionSupport.SUCCESS;
	}
}














