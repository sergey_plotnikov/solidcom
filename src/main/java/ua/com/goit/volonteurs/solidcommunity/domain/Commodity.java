package ua.com.goit.volonteurs.solidcommunity.domain;

import java.io.Serializable;

public class Commodity implements Serializable {

    private static final long serialVersionUID = -3025655480291389465L;
    
    private long id;
    private String name;
    private float price;
    private float nonSeasonPrice;
    private String measure;
    private String description;

    public Commodity() {

    }

    public Commodity(long id, String name, float price, float nonSeasonPrice) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
        this.nonSeasonPrice = nonSeasonPrice;

    }

    @Override
    public String toString() {
        return "Commodity [id=" + id + ", name=" + name + ", price=" + price
                + ", nonSeasonPrice=" + nonSeasonPrice + "]";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setPrice(String price) {
    	this.price = parseFloat(price);
    }
    
    public float getNonSeasonPrice() {
        return nonSeasonPrice;
    }

    public void setNonSeasonPrice(String nonSeasonPrice) {
    	this.nonSeasonPrice = parseFloat(nonSeasonPrice);
    }
    
    public void setNonSeasonPrice(float nonSeasonPrice) {
        this.nonSeasonPrice = nonSeasonPrice;
    }

    public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private float parseFloat(String line) {
		float number = Float.parseFloat(line);
		return number;
	}
}
