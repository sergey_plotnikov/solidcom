/**
 * 
 */
package ua.com.goit.volonteurs.solidcommunity.web.actions.response;

import java.util.List;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.ResponseDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;
import ua.com.goit.volonteurs.solidcommunity.services.Paginator;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author SASH
 *
 */
public class ResponseList extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 6447127759961535645L;
    private static final Logger LOG = Logger.getLogger(ResponseList.class);
    
    private ResponseDao responseDao = null;
    private Paginator paginator = new Paginator();
	private List<Response> responses;
    
	 public String execute() {
	    	
    	paginator.checkAction();
		
    	try {
            retrieve(paginator);
            return ActionSupport.SUCCESS;
        } catch (SolidComDaoException e) {
            LOG.error("Can't retrieve response list: " + e.getMessage(), e);
            addActionError("Can't retrieve response list!");
            return ActionSupport.ERROR;
        }
    	
    }
	 
	private void retrieve(Paginator paginator) throws SolidComDaoException {
		responseDao = ApplicationContextProvider.getApplicationContext().getBean(ResponseDao.class);
		responses = responseDao.retrieveAll(paginator);
	} 
	    
    public ResponseDao getResponseDao() {
        return responseDao;
    }
    public void setResponseDao(ResponseDao responseDao) {
        this.responseDao = responseDao;
    }
    public List<Response> getResponses() {
        return responses;
    }
    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }
    public Paginator getPaginator() {
		return paginator;
	}
	public void setPaginator(Paginator paginator) {
		this.paginator = paginator;
	}
}
