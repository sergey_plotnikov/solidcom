package ua.com.goit.volonteurs.solidcommunity.web.actions.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;
import ua.com.goit.volonteurs.solidcommunity.services.ResponseService;

import com.opensymphony.xwork2.ActionSupport;

public class RequestDetails extends ActionSupport {
	
	private static final Logger LOG = Logger.getLogger(RequestDetails.class);
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private String commodityName;
	private int quantity;
	private String description;
	private List<Response> responses = new ArrayList<Response>();
	
	public RequestDetails() {
		// TODO Auto-generated constructor stub
	}
	
	public String execute(){
		
		RequestDao requestDao = ApplicationContextProvider.getApplicationContext().getBean(RequestDao.class);
		ResponseService service = new ResponseService();
		Request request = null;
		try {
			request = requestDao.retrieve(id);
            this.responses = service.getAllResponses(id);
		} catch (SolidComDaoException e) {
			LOG.error("Can't find request:" + e.getMessage(), e);
			addActionError("Can't find request!");
		}
		
		name = request.getName();
		quantity = request.getQuantity();
		description = request.getDescription();
		commodityName = request.getCommodity().getName();
		
		return ActionSupport.SUCCESS;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }

	

}
