/**
 * 
 */
package ua.com.goit.volonteurs.solidcommunity.web.actions.response;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.ResponseDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;
import ua.com.goit.volonteurs.solidcommunity.domain.ResponseStatus;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author SASH
 *
 */
public class ResponseUpdater extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 6644741527268697774L;
    private static final Logger LOG = Logger.getLogger(ResponseUpdater.class);
    
    private long id;
    private String name;
    private String description;
    private int requestId;
    private ResponseStatus status;
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the requestId
     */
    public int getRequestId() {
        return requestId;
    }
    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
    /**
     * @return the status
     */
    public ResponseStatus getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(ResponseStatus status) {
        this.status = status;
    }
    
    public String execute() throws Exception {
        Response response = new Response();
        response.setId(id);
        response.setName(name);
        response.setDescription(description);
        response.setStatus(status);
        try {
            RequestDao requestDao = ApplicationContextProvider.getApplicationContext().getBean(RequestDao.class);
            Request request = requestDao.retrieve(requestId);
            response.setRequest(request);
            ResponseDao responseDao = ApplicationContextProvider.getApplicationContext().getBean(ResponseDao.class);
            responseDao.update(response);
            return ActionSupport.SUCCESS;
        } catch (SolidComDaoException e) {
            LOG.error("Can't update response: " + e.getMessage(), e);
            addActionError("Can't update response!");
            return ActionSupport.ERROR;
        }
    }
}
