package ua.com.goit.volonteurs.solidcommunity.web.actions.commodity;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.CommodityDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;

import com.opensymphony.xwork2.ActionSupport;

public class CommodityCreater extends ActionSupport {

	private static final long serialVersionUID = -8806285455060309352L;
	private static final Logger LOG = Logger.getLogger(CommodityCreater.class);
	
	private String name;
	private String price;
	private String nonSeasonPrice;
	private String measure;
	private String description;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getNonSeasonPrice() {
		return nonSeasonPrice;
	}

	public void setNonSeasonPrice(String nonSeasonPrice) {
		this.nonSeasonPrice = nonSeasonPrice;
	}
	
	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String execute() throws Exception {
		Commodity commodity = new Commodity();
		commodity.setName(name);
		commodity.setPrice(price);
		commodity.setNonSeasonPrice(nonSeasonPrice);
		commodity.setMeasure(measure);
		commodity.setDescription(description);
		CommodityDao dao = new CommodityDao();
		try {
			dao.save(commodity);
		} catch (SolidComDaoException e) {
			LOG.error("Can not create Commodity: " + e.getMessage(), e);
			addActionError("Can not create Commodity!");
			return ActionSupport.ERROR;
		}
		return ActionSupport.SUCCESS;
	}
}














