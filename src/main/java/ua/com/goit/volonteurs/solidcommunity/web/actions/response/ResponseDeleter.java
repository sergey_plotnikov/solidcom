/**
 * 
 */
package ua.com.goit.volonteurs.solidcommunity.web.actions.response;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.ResponseDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author SASH
 *
 */
public class ResponseDeleter extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 6638400276310776758L;
    private static final Logger LOG = Logger.getLogger(ResponseDeleter.class);
    
    private long id;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    public String execute() throws Exception {
        ResponseDao responseDao = new ResponseDao();
        Response response = null;
        try {
            response = responseDao.retrieve(id);
        } catch (NumberFormatException | SolidComDaoException e) {
            LOG.error("Can't retrieve response by id: " + e.getMessage(), e);
            addActionError("Can't retrieve response by id!");
        }
        try {
            responseDao.delete(response);
            return ActionSupport.SUCCESS;
        } catch (SolidComDaoException e) {
            LOG.error("Can't delete response " + e.getMessage(), e);
            addActionError("Can't delete response!");
            return ActionSupport.ERROR;
        }
    }
}
