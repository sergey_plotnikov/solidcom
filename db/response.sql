﻿CREATE TABLE response
(
  id bigserial NOT NULL PRIMARY KEY,
  name text NOT NULL,
  description text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE response
  OWNER TO GOIT;